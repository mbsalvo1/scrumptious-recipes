from django.contrib import admin
from .models import FoodItem
from .models import Measurement
from .models import Ingredient
from .models import Recipe
from .models import Step

# Register your models here.

admin.site.register(Recipe)
admin.site.register(Measurement)
admin.site.register(FoodItem)
admin.site.register(Ingredient)
admin.site.register(Step)
